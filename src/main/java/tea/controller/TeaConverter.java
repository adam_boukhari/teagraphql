package tea.controller;

import org.springframework.stereotype.Component;

import tea.api.TeaDto;
import tea.domaine.Tea;


@Component
public class TeaConverter {

	public TeaDto fromTea(Tea tea) {
		return new TeaDto(tea.getTeaId(), tea.getTeaName(), tea.getTeaCategory(), tea.getTeaDescription());
	}

	public Tea toTea(TeaDto dto) {
		return new Tea(dto.teaId, dto.teaName, dto.teaCategory, dto.teaDescription);
	}

}
