package tea.infra;

import org.springframework.stereotype.Component;

import tea.domaine.Role;
import tea.domaine.User;

@Component
public class UserEntityConverter {
	
	public UserEntity fromUser(User user) {
		return new UserEntity(user.getId(), 
				user.getFirstname(),
				user.getLastname(),
				user.getEmailAddress(),
				user.getPassword(),
				fromRole(user.getRole()));
	}
	
	public User toUser(UserEntity user) {
		return new User(user.id, 
				user.firstname,
				user.lastname,
				user.email,
				user.password,
				toRole(user.role));
	}
	
	public RoleEntity fromRole(Role role) {
		return new RoleEntity(role.getIdentifier(), role.getRoleName());
	}
	
	public Role toRole(RoleEntity role) {
		return new Role(role.id, role.roleName);
	}
}
