package tea.infra;

import org.springframework.stereotype.Component;

import tea.domaine.Tea;

@Component
public class TeaEntityConverter {
	
	public TeaEntity fromTea(Tea tea) {
		return new TeaEntity(tea.getTeaId(), tea.getTeaName(), tea.getTeaCategory(), tea.getTeaDescription());
	}
	
	public Tea toTea(TeaEntity entity) {
		return new Tea(entity.teaId, entity.teaName, entity.teaCategory, entity.teaDescription);
	}
}
